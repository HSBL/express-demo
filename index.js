const express = require('express')
const app = express()
const port = 80

// application level middleware
const logger = (req, res, next) => {
  // logic middleware
  console.log(`${req.method} ${req.url}`)
  
  next()
}

app.use(logger)

// built in middleware
app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.get('/', (req, res) => res.send('Hello Express!!!'))

app.get('/users', (req, res) => {
  res.status(404).send("User not found!")
})

app.get('/products', (req, res) => {
  res.json(["apple", "redmi", "one Plus"])
})

app.get('/orders', (req, res) => {
  res.json([
    {
      id: 1,
      paid: true,
      user_id: 1
    },
    {
      id: 2,
      paid: false,
      user_id: 1
    },
  ])
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
